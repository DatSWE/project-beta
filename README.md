# CarCar

CarCar is an application for managing the aspects of an automobile dealership. It manages the inventory, automobile sales, and automobile services.

## Team

**Dat Le** - Services

**Megan Stewart** - Sales

## Getting Started

1. Fork and clone project repo: https://gitlab.com/DatSWE/project-beta

2. CD into project directory

3. Open docker desktop

4. In your project directory run these docker commands to build volumes, images and containers:
```
docker volume create beta-data
docker-compose build
docker-compose up
```
5. View project in browser: http://localhost:3000/

## Design

CarCar has 3 microservices which interact with one another where Services and Sales poll from Inventory to keep up to date on cars-for-sale data.

    - Inventory
    - Services
    - Sales

![Img](/images/CarCarDiagram.png)

## Inventory microservice

Houses the automobile, manufacturer and vehicle model entities with GET DELETE PUT methods for each

### Views, Endpoints, Methods

### Manufacturers

> List manufacturers | GET | http://localhost:8100/api/manufacturers/
>
> Create a manufacturer | POST	| http://localhost:8100/api/manufacturers/
>
> Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/:id/
>
> Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/:id/
>
> Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/:id/

### Vehicle Models

> List models | GET | http://localhost:8100/api/models/
>
> Create a model | POST	| http://localhost:8100/api/models/
>
> Get a specific model | GET | http://localhost:8100/api/models/:id/
>
> Update a specific model | PUT | http://localhost:8100/api/models/:id/
>
> Delete a specific model | DELETE | http://localhost:8100/api/models/:id/

### Automobiles

> List automobiles | GET | http://localhost:8100/api/automobiles/
>
> Create a automobile | POST	| http://localhost:8100/api/automobiles/
>
> Get a specific automobile | GET | http://localhost:8100/api/automobiles/:id/
>
> Update a specific automobile | PUT | http://localhost:8100/api/automobiles/:id/
>
> Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/:id/

## Ports
**Configure routes to Access Form on Browser HERE:**
>
> **manufacturers List:** http://localhost:3000/manufacturers/
>
> **manufacturers add:** http://localhost:3000/manufacturers/add/
>
> **models list:** http://localhost:3000/models/
>
> **models add:** http://localhost:3000/models/add/
>
> **automobiles list:** http://localhost:3000/automobiles/
>
> **automobiles add:** http://localhost:3000/automobiles/add/

## Service microservice

The service microservice will have models for Technician and an AutomobileVO, and an Appointment model. The Technician model will track the following properties like first_name,last_name, and the employee_id. The AutomobileVO model contains vin and sold fields. The appointment contains  date_time, reason,status,vin(CharField),customer, and a technician field(Foreign key).
​
The AutomobileVO is a value object that will be using a poller to get the data about the autombile from the inventory evvery 60 sec with the updated VINs.
​
### Accessing Endpoints to Send and View Data: Access Through Innomnia & Broswer

> List technicians | GET | http://localhost:8080/api/technicians/
>
> Create a technician | POST | http://localhost:8080/api/technicians/
>
> Delete a technician | DELETE | http://localhost:8080/api/technicians/:id
>
> List appointments | GET | http://localhost:8080/api/appointments/
>
> Create an appointment | POST| http://localhost:8080/api/appointments/
>
> Delete an appointment| DELETE| http://localhost:8080/api/appointments/:id
>
> Set appointment status to "canceled"| PUT| http://localhost:8080/api/appointments/:id/cancelappointments/:id
>
> Set appointment status to "finished"| PUT| http://localhost:8080/api/appointments/:id/finish

### JSON body response/request

**Create a technician Example | POST | JSON body**
```
{
	"first_name": "Liz",
    "last_name": "Lizzy",
	"employee_number": 2
}
```
**Create an appointment Example | POST | JSON body**
```
{
    "id": 6,
    "vin": "1222",
    "customer_name": "Gary",
    "time": "12:30:00",
    "date": "2021-07-11",
    "reason": "new car",
    "vip_status": false,
    "technician": "Caleb"
}
```
#### Poller

Services microservice polls from inventory microserice for automobile for sale data. The services microservice will poll for automobile data *every 60 second* and update the AutomobileVO with updated VINs in order verify VIP status on appointments with associated sold = TRUE vin numbers.

**Poll from:** http://inventory-api:8000/api/automobiles/

### Frontend

**Service Technician |**

1. Add a Technician Form
    - input first name
    - input last name
    - input employee ID

**Configure routes to Access Form on Browser HERE:** http://localhost:3000/technician/add/

2. List Technicians
    - employee ID
    - first name
    - last name

**Configure routes to Access List on Browser HERE:** http://localhost:3000/technician/list/

**Appointments |**

1. Add an Appointment Form
    - input vin
    - input customer
    - input date/time
    - input technician (dropdown)
    - input reason

**Configure routes to Access Form on Browser HERE:** http://localhost:3000/appointment/reserve/

2. List Appointments (cancel and finish status buttons *special feature*)
    - vin
    - VIP status
    - customer
    - Date
    - Time
    - Technician
    - reason

**Configure routes to Access List on Browser HERE:** http://localhost:3000/appointment/list/

**Service History |**

1. List all Services (Search VIN for vip status *special feature*)
    - vin
    - VIP status
    - customer
    - Date
    - Time
    - Technician
    - reason
    - Status (finished or cancelled)


## Sales Microservice

### Backend

#### Models

1. AutomobileVO **polls from inventory automobile data**
    - vin
    - sold (boolean)

2. SalesPerson
    - first_name
    - last_name
    - employee_id

3. Customer
    - first_name
    - last_name
    - address
    - phone_number

4. Sale
    - automobile *ForeignKey*
    - salesperson *ForeignKey*
    - customer *ForeignKey*
    - price
    *IMPORTANT NOTE | only delete sales through the delete sale method, no FK model instance deletion should remove the record of a sale*

#### Views, Endpoints and Methods

*view in browser and configure in Insomnia to test APIs*
>
> | List SalesPeople | GET | http://localhost:8090/api/salespeople/
>
> | Create a SalesPerson | POST | http://localhost:8090/api/salespeople/
>
> | Delete a specific SalesPerson | DELETE | http://localhost:8090/api/salespeople/:id
>
> | List customers | GET | http://localhost:8090/api/customers/
>
> | Create a customer | POST | http://localhost:8090/api/customers/
>
> | Delete a specific customer | DELETE | http://localhost:8090/api/customers/id/
>
> | List sales | GET | http://localhost:8090/api/sales/
>
> | Create a sale | POST | http://localhost:8090/api/sales/
>
> | Delete a specific sale | DELETE | http://localhost:8090/api/sales/:id

### JSON body POST requests

**Create a SalesPerson Example | POST | JSON body**
```
{
    "first_name": "Jane",
    "last_name": "Doe",
    "employee_id": 1
}
```
**Create a Customer Example | POST | JSON Body**
```
{
    "first_name": "Jane",
    "last_name": "Doe",
    "address": "123 Big Bird St",
    "phone_number": 1234567890
}
```
**Create a Sale Example | POST | JSON body**
```
{
	"salesperson": 1, (by id)
	"customer": 2, (by id)
	"vin": "888",
	"price": 40000
}
```

#### Poller

Sales microservice polls from inventory microserice for automobile for sale data. The sales microservice will poll for automobile data *every second* and update the AutomobileVO with updated VINs in order to record a sale on available automobiles with a sold = FALSE status.

**Poll from:** http://inventory-api:8000/api/automobiles/

### Frontend

**SalesPerson |**

1. Add a SalesPerson Form
    - input firstName
    - input lastName
    - input employeeId

**Configure routes to Access Form on Browser HERE:** http://localhost:3000/salespeople/add/

2. List Sales people
    - employee ID
    - first name
    - last name

**Configure routes to Access List on Browser HERE:** http://localhost:3000/salespeople/

**Customer |**

1. Add a Customer Form
    - imput first name
    - input last name
    - input address
    - input phone number

**Configure routes to Access Form on Browser HERE:** http://localhost:3000/customers/add/

2. List Customers
    - first name
    - last name
    - address
    - phone number

**Configure routes to Access List on Browser HERE:** http://localhost:3000/customers/

**Sale |**

1. Record a new sale Form
    - automobile VIN dropdown selection input (UNSOLD INVENTORY ONLY)
    - salesperson dropdown selection input
    - customer dropdown selection input
    - price input

**Configure routes to Access Form on Browser HERE:** http://localhost:3000/sales/add

2. List all sales
    - salesperson employee ID
    - salesperson fullname
    - customer fullname
    - VIN **(of car sold)** *special feature*
    - price
    - *special feature to search by salesperson to view list of salesperson sales history*

**Configure routes to Access Form on Browser HERE:** http://localhost:3000/sales/
