# Generated by Django 4.0.3 on 2023-07-26 18:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0010_remove_salesperson_name_salesperson_first_name_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='salesperson',
            name='employee_id',
            field=models.CharField(max_length=50, null=True, unique=True),
        ),
    ]
