import React, { useState, useEffect } from 'react';


function ModelList() {
    const [models, setModels] = useState([]);
    

    async function load_Models() {
          const response = await fetch('http://localhost:8100/api/models/')
          if (response.ok){
            const data = await response.json()
            setModels(data.models)
          }
        }
    useEffect(() => {
        load_Models();
    }, [])

    return (
      <div>
      <h1>Models</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col"> Name </th>
            <th scope="col"> Manufacturer </th>
            <th scope="col"> Picture </th>
            </tr>
        </thead>
        <tbody>
          {models.map(model => {
            return (
              <tr key={model.name}>
                <td>{model.name}</td>
                <td>{model.manufacturer.name }</td>
                <td><img src = {model.picture_url} width={90} height={90}  alt ="picture of vehicles model"/></td>
            </tr>
            );
          })}
        </tbody>
      </table>
      </div>
    );
  }

  export default ModelList;
