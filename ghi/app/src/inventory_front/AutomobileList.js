import React, { useState, useEffect } from 'react';


function AutoList() {
    const [autos, setAutos] = useState([]);

    async function loadAutos() {
          const response = await fetch ('http://localhost:8100/api/automobiles/')
          if (response.ok){
            const data = await response.json();
            setAutos(data.autos);
        }
    }

    useEffect(() => {
        loadAutos();
    }, [])

    const soldCars = autos.filter(auto => auto.sold === true);
    const availableCars = autos.filter(auto => auto.sold === false);

    return (
      <div>
      <h1>Automobile Inventory</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col"> VIN </th>
            <th scope="col"> Color </th>
            <th scope="col"> Year </th>
            <th scope="col"> Model </th>
            <th scope="col"> Manufacture </th>
            <th scope="col"> Status </th>
          </tr>
        </thead>
        <tbody>
          {soldCars.map(auto => {
            return (
              <tr key={auto.href}>
                <td>{ auto.vin }</td>
                <td>{ auto.color }</td>
                <td>{ auto.year }</td>
                <td>{ auto.model.name }</td>
                <td>{ auto.model.manufacturer.name }</td>
                <td>SOLD</td>
              </tr>
            );
          })}
          {availableCars.map(auto => {
            return (
              <tr key={auto.href}>
                <td>{ auto.vin }</td>
                <td>{ auto.color }</td>
                <td>{ auto.year }</td>
                <td>{ auto.model.name }</td>
                <td>{ auto.model.manufacturer.name }</td>
                <td>AVAILABLE</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </div>
    );
  }

export default AutoList;
