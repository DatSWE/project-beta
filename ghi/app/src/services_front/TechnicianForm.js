import React, { useState, useEffect } from 'react';


function TechnicianForm(props) {
  const [first_name,setfirstName]= useState('');
  const [last_name, setlastName] = useState('');
  const [employee_id, setEmployeeId] = useState('');
  const [add,SetAdd] = useState(false);

  async function fetchTechnician() {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

    }
  }

  useEffect(() => {
    fetchTechnician();
  }, [])

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      first_name,
      last_name,
      employee_id,
    };
    const technincianUrl = 'http://localhost:8080/api/technicians/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(technincianUrl, fetchConfig);
    console.log(response);
    if (response.ok) {
      const newTechnician = await response.json();
      setfirstName('');
      setlastName('');
      setEmployeeId('');
      SetAdd(true);
      
     
    }
 }

  function handleFirstNameChange(event) {
    const  value  = event.target.value;
    setfirstName(value);
  }

  function handleLastNameChange(event) {
    const value  = event.target.value;
    setlastName(value);
  }

  function handleEmployeeIdChange(event) {
    const value  = event.target.value;
    setEmployeeId(value);
  }

  let successMessage = 'alert alert-success d-none mb-0';
  let TechnicianForm= '';
  if (add) {
    successMessage = 'alert alert-success mb-0';
    TechnicianForm = 'd-none';
}

return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Technician</h1>
          <form  className = {TechnicianForm} onSubmit = {handleSubmit} id="Add-Technician-Form">
            <div className="form-floating mb-3">
              <input onChange ={handleFirstNameChange} placeholder="first_Name" required type="text" name="first_name" id="first_Name" className="form-control" />
              <label htmlFor="name">First name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange ={handleLastNameChange} placeholder="last_Name" required type="text" name="last_name" id="last_Name" className="form-control" />
              <label htmlFor="starts">Last name</label>
            </div>
            <div className="form-floating mb-3">
              <input  onChange ={handleEmployeeIdChange} placeholder="employee_id" required type="text" name="employee_id" id="employee_id" className="form-control" />
              <label htmlFor="ends">Employee ID</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
           <div className={successMessage} id="success-message">
              Your Technician has been created!
            </div>
          </div>
      </div>
    </div>
  );
}

export default TechnicianForm;
