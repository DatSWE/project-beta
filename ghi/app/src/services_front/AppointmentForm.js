import React, { useState, useEffect } from 'react';

function Appointment_Form() {
  const [vin, setVin] = useState('');
  const [customer, setCustomer] = useState('');
  const [date_time, setDateTime] = useState('');
  const [technicians, setTechnicians] = useState([]);
  const [technician, setTechnician] = useState('');
  const [reason,setReason] = useState('')
  const [add,setAdd]= useState(false);

  async function fetchTechnician() {
    const url = 'http://localhost:8080/api/technicians/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    console.log(data)
    }
  }

  useEffect(() => {
    fetchTechnician();
  }, [])

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {}
    data.vin = vin
    data.customer = customer
    data.date_time = date_time
    data.technician = technician
    data.reason = reason



    const appointmentUrl= 'http://localhost:8080/api/appointments/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
      const newAppointment = await response.json();
      console.log(newAppointment)
      setVin('');
      setCustomer('');
      setDateTime('');
      setTechnician('');
      setAdd(true);
      setReason('');
    }
  }

  function handleChangeVin(event) {
    const { value } = event.target;
    setVin(value);
  }

  function handleChangeCustomer(event) {
    const { value } = event.target;
    setCustomer(value);
  }

  function handleChangeDate(event) {
    const { value } = event.target;
    setDateTime(value);
  }

 function handleChangeTechnician(event) {
    const { value } = event.target;
    setTechnician(value);
  }


  function handleChangeReason(event) {
    const { value } = event.target;
    setReason(value);
  }

    let successMessage = 'alert alert-success d-none mb-0';
    let AppointmentForm = '';
    if(add) {
    successMessage = 'alert alert-success mb-0';
    AppointmentForm = 'd-none';
}

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Reserve a service appointment</h1>
          <form className = {AppointmentForm} onSubmit={handleSubmit} id="Create-Appointment-form">
            <div className="form-floating mb-3">
              <input value={vin} onChange={handleChangeVin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
              <label htmlFor="style">Automobile VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input value={customer} onChange={handleChangeCustomer} placeholder="customer" required type="text" name="customer" id="customer" className="form-control" />
              <label htmlFor="color">Customer</label>
            </div>
            <div className="form-floating mb-3">
              <input value={date_time} onChange={handleChangeDate} placeholder="date_time" required type="datetime-local" name="date_time" id="date_time" className="form-control" />
              <label htmlFor="picture_url">Date/Time</label>
            </div>
            <div className="mb-3">
              <select value={technician} onChange={handleChangeTechnician} required name="technician" id="technician" className="form-select">
                <option >Choose a technician</option>
                {technicians.map(technician => {
                  return (
                    <option key={technician.id} value={technician.id}>{technician.first_name} {technician.last_name}</option>
                  )
                })}
              </select>
            </div>
            <div className = "form-floating mb-3">
            <input value={reason} onChange={handleChangeReason} placeholder="reason" required type="text" name="reason" id="reason" className="form-control" />
              <label htmlFor="reason">Reason</label>
            </div>
            <button className="btn btn-primary">Reserve!</button>
          </form>
          <div className={successMessage} id="success-message">
              Your appointment has be reserved!
            </div>
        </div>
      </div>
    </div>
  );
}

export default Appointment_Form;
