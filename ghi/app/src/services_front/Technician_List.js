import React,{useState, useEffect} from 'react';

function Technician_List(){
    const [technicians, setTechnicians] = useState([]);

    async function loadTechnician() {
        const url = 'http://localhost:8080/api/technicians/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();

            setTechnicians(data.technicians);
        } else {
            console.log(response)
        }
      }

    useEffect(() => {
        loadTechnician();
      }, [])

      return (

        <table className="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Employee Id</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    
                </tr>
            </thead>
            <tbody>
                {technicians.map(technician => {
                    return (
                        <tr key={technician.employee_id}>
                            <td>{ technician.employee_id }</td>
                            <td>{ technician.first_name}</td>
                            <td>{ technician.last_name }</td>
                        </tr>
                        );
                        })}
   
            </tbody>
        </table>
    );
  }


  export default Technician_List;
