import React,{useState, useEffect} from 'react';


function Appointment_List(){
    const [appointments, setAppointments] = useState([]);
    

    async function loadAppointments() {
        const url = 'http://localhost:8080/api/appointments/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();

            setAppointments(data.appointments);
        } else {
            console.log(response)
        }
      }
    
    async function cancelAppointment(appointment_Id){
      const appointment_url =`http://localhost:8080/api/appointments/${appointment_Id}/`;
      const fetchOptions={
      method:"PUT",
      body:JSON.stringify({status:"Cancelled"}),
      headers:{'Content-Type':'application/json'}
  }
      const response = await fetch(appointment_url,fetchOptions)
      

      if(response.ok){
        loadAppointments()
      }
  }

    async function finishAppointment(appointment_Id){
      const appointment_url =`http://localhost:8080/api/appointments/${appointment_Id}/`;
      const fetchOptions={
      method:"PUT",
      body:JSON.stringify({status:"Finished"}),
      headers:{'Content-Type':'application/json'}
  }
    const response = await fetch(appointment_url,fetchOptions)
      
    if(response.ok){
      loadAppointments()
  }
}



    useEffect(() => {
      loadAppointments();
      
  },[])

    
return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody>
        {appointments.filter((appointment)=>{
            return appointment.status === "Made";
          }).map(appointment => {
            return (
              <tr key={appointment.id}>
                <td>{ appointment.vin }</td>
                <td> {appointment.is_vip ? "Yes" : "No" } </td>
                <td>{ appointment.customer }</td>
                <td> { new Date (appointment.date_time).toLocaleDateString() } </td>
                <td> { new Date (appointment.date_time).toLocaleTimeString() } </td>
                <td>{appointment.technician.first_name}</td>
                <td>{ appointment.reason }</td>
                <td>
                  <button  className="btn btn-success" onClick={() => finishAppointment(appointment.id)}>Finish</button>
                </td>
                <td>
                  <button className="btn btn-danger" onClick={() => cancelAppointment(appointment.id)}>Cancel</button>
                </td>
               
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }


  export default Appointment_List;
