import React, { useState, useEffect } from 'react';


function SalesForm({getSales}) {
    const[autos, setAutos] = useState([]);
    const[auto, setAuto] = useState('');
    const[salesPeople, setSalesPeople] = useState([]);
    const[salesPerson, setSalesPerson] = useState('');
    const[customers, setCustomers] = useState([]);
    const[customer, setCustomer] = useState('');
    const[price, setPrice] = useState('');

    async function fetchAutos() {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            const autosForSale = data.autos.filter((auto) => auto.sold === false);
            setAutos(autosForSale);
            setAuto(data.autos.vin);
        }
    }

    async function fetchSalesPeople() {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalesPeople(data.salespeople);
        }
    }

    async function fetchCustomers() {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    }

    useEffect(() => {
        fetchAutos();
        fetchSalesPeople();
        fetchCustomers();
    }, [])

    async function handleSubmit(event) {
        event.preventDefault();
        const saleData = {
            automobile: auto,
            sales_person: salesPerson,
            customer,
            price,
        };
        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchSaleConfig = {
            method: "post",
            body: JSON.stringify(saleData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const autoData = {
            sold: true,
        };
        const autoUrl = `http://localhost:8100/api/automobiles/${auto}/`;
        const fetchAutoConfig = {
            method: "PUT",
            body: JSON.stringify(autoData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const saleResponse = await fetch(salesUrl, fetchSaleConfig);
        const autoResponse = await fetch(autoUrl, fetchAutoConfig);
        if (saleResponse.ok) {
            setAuto('');
            setSalesPerson('');
            setCustomer('');
            setPrice('');
            getSales();
        }
        if (!autoResponse.ok) {
            console.log("Sold status didn't update!!");
        }
    }

    function handleChangeAuto(event) {
        const {value} = event.target;
        setAuto(value);
    }

    function handleChangeSalesPerson(event) {
        const {value} = event.target;
        setSalesPerson(value);
    }

    function handleChangeCustomer(event) {
        const {value} = event.target;
        setCustomer(value);
    }

    function handleChangePrice(event) {
        const {value} = event.target;
        setPrice(value);
    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Record a Sale</h1>
              <form onSubmit={handleSubmit} id="add-salesperson-form">
              <div className="mb-3">
                    <label htmlFor="auto">Automobile Vin Number</label>
                    <select value={auto} onChange={handleChangeAuto} required name="auto" id="auto" placeholder="Automobile Vin Number" className="form-select">
                        <option value="">Select an Automobile Vin Number</option>
                        {autos.map(auto => {
                        return (
                            <option key={auto.id} value={auto.vin}>{auto.vin}</option>
                        )
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <label htmlFor="sales_person">Salesperson</label>
                    <select value={salesPerson} onChange={handleChangeSalesPerson} required name="salesPerson" id="sales_person" placeholder="Salesperson" className="form-select">
                        <option value="">Select a Salesperson</option>
                        {salesPeople.map(person => {
                        return (
                            <option key={person.id} value={person.id}>{person.first_name} {person.last_name}</option>
                        )
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <label htmlFor="customer">Customer</label>
                    <select value={customer} onChange={handleChangeCustomer} required name="customer" id="customer" placeholder="Customer" className="form-select">
                        <option value="">Select a Customer</option>
                        {customers.map(person => {
                        return (
                            <option key={person.id} value={person.id}>{person.first_name} {person.last_name}</option>
                        )
                        })}
                    </select>
                </div>
                <label htmlFor="price">Price</label>
                <div className="form-floating mb-3">
                    <input value={price} onChange={handleChangePrice} placeholder="Price" required type="text" name="price" id="price" className="form-control" />
                    <label htmlFor="price">Set a Price</label>
                </div>
                <button className="btn btn-primary">Record</button>
              </form>
            </div>
          </div>
        </div>
    );
}

export default SalesForm;
