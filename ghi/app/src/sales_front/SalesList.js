import React, { useState, useEffect } from 'react';

function SalesList() {
    const[salesPeople, setSalesPeople] = useState([]);
    const[salesPerson, _] = useState('');
    const[sales, setSales] = useState([]);
    const[allSales, setAllSales] = useState([]);

    async function fetchSalesPeople() {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalesPeople(data.salespeople);
        }
    }

    async function fetchSales() {
        const url = 'http://localhost:8090/api/sales/';
        const response = await fetch(url);
        if (response.ok) {
			const data = await response.json();
			setSales(data.sales);
            setAllSales(data.sales);
		}
    }

    useEffect(() => {
        fetchSalesPeople();
        fetchSales();
    }, []);

    function handleChangeSalesPerson(event) {
        const {value} = event.target;
        if (value !== '') {
            const filteredSales = allSales.filter((s) => s.sales_person.employee_id === value);
            setSales(filteredSales);
        }
    }

    return (
        <div>
        <h1>Sales History</h1>
            <div className="mb-3">
                <label htmlFor="sales_person">Search by Salesperson to view their Sales History</label>
                <select value={salesPerson} onChange={handleChangeSalesPerson} required name="salesPerson" id="sales_person" placeholder="Salesperson" className="form-select">
                    <option value="">Select a Salesperson</option>
                    {salesPeople.map(person => {
                    return (
                        <option key={person.id} value={person.employee_id}>{person.first_name} {person.last_name}</option>
                    )
                    })}
                </select>
            </div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Salesperson Employee ID</th>
                    <th>Salesperson Name</th>
                    <th>Customer</th>
                    <th>Vin</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {sales.map(sale => {
                    return (
                        <tr key={sale.id}>
                            <td>{sale.sales_person.employee_id}</td>
                            <td>{sale.sales_person.first_name} {sale.sales_person.last_name}</td>
                            <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.price}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
    )
}

export default SalesList;
