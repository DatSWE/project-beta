
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import *


class TechniciansListEncoder(ModelEncoder):
    model = Technicians
    properties = ["first_name",
                  "last_name",
                  "employee_id",
                  "id",]


class AutomobileVOListEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["import_href",
                  "vin",
                  "sold"]

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = ["date_time",
                  "reason",
                  "status",
                  "customer",
                  "technician",
                  "id",
                  "vin",
                  "is_vip",
                  ]

    encoders = {"technician": TechniciansListEncoder()}


@require_http_methods(["GET","POST"])
def list_technician(request):
    if request.method == "GET":
        technicians = Technicians.objects.all()
        return JsonResponse({'technicians':technicians},encoder = TechniciansListEncoder,safe=False)
    
    else:
        try:
            content = json.loads(request.body)
            technician = Technicians.objects.create(**content)
            return JsonResponse(technician,encoder = TechniciansListEncoder,safe=False)
        except:
            return JsonResponse({"message":"Erro cannot create technician"},status=400)


@require_http_methods(["GET","DELETE"])  
def detail_technician(request,id):
    if request.method == "GET":
        technicians = Technicians.objects.get(id=id)
        return JsonResponse(technicians,encoder=TechniciansListEncoder,safe =False)
    else:
        count,_ =Technicians.objects.filter(id=id).delete()
        return JsonResponse({"Deleted":count >0})


@require_http_methods(["GET","POST"])  
def list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse({'appointments':appointments},encoder=AppointmentListEncoder,safe=False)
    else:
        content = json.loads(request.body)
        try:
            technician = Technicians.objects.get(id=content['technician'])
            content['technician'] = technician
        except Technicians.DoesNotExist:
            return JsonResponse({'message':"Invalid Technician Id"},status=400)  
        vin = content['vin']
        if AutomobileVO.objects.filter(vin=vin).count()==1:
            content["is_vip"] =True
        appointment = Appointment.objects.create(**content)
        return JsonResponse(appointment,encoder = AppointmentListEncoder,safe=False)   



@require_http_methods(["GET","DELETE","PUT"])
def detail_appointments(request,id):
    if request.method == "GET":
        appointments = Appointment.objects.get(id=id)
        return JsonResponse(appointments,encoder=AppointmentListEncoder,safe=False)
    
    elif request.method == "PUT":
        content=json.loads(request.body)
        if content['status'] == "Cancelled":
            Appointment.objects.filter(id=id).update(**content)
            appointments = Appointment.objects.get(id=id)
            return JsonResponse(appointments,encoder=AppointmentListEncoder,safe=False)
        

        if content['status'] == 'Finished':
            Appointment.objects.filter(id=id).update(**content)
            appointments = Appointment.objects.get(id=id)
            return JsonResponse(appointments,encoder=AppointmentListEncoder,safe=False)

    else:
        count,_ =Appointment.objects.filter(id=id).delete()
        return JsonResponse({'deleted':count >0})
