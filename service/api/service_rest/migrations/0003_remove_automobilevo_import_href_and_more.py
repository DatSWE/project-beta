# Generated by Django 4.0.3 on 2023-07-25 19:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0002_alter_appointment_status'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='automobilevo',
            name='import_href',
        ),
        migrations.AlterField(
            model_name='technicians',
            name='employee_id',
            field=models.CharField(max_length=200, unique=True),
        ),
    ]
