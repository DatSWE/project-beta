from django.db import models


class Technicians(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class AutomobileVO(models.Model):
    href = models.CharField(max_length=200, unique=200)
    vin = models.CharField(max_length=200)
    sold = models.CharField(max_length=200)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.TextField(max_length=200)
    status = models.TextField(max_length=200, default="Made")
    vin = models.CharField(max_length=200)
    is_vip = models.BooleanField(default=False, null=True, blank=True)
    customer = models.CharField(max_length=200)
    technician = models.ForeignKey(
        Technicians,
        related_name="appointment",
        on_delete=models.CASCADE,
    )
